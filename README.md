### Enjoy

Enjoy 是基于 Java 语言的极轻量极魔板引擎。极轻量级仅 227 KB 并且不依赖任何第三方。极简设计仅 if、for、switch、set、define、include、render 七个核心指令，让学习成本低到极致。独创 DKFF(Dynamic Key Feature Forward) 词法分析算法与 DLRD (Double Layer Recursive Descent)语法分析算法，避免使用 javacc、antlr、jflex 生成器，令代码量少到极致。

#### Maven 坐标

```java
<dependency>
    <groupId>com.jfinal</groupId>
    <artifactId>enjoy</artifactId>
    <version>4.8</version>
</dependency>
```

#### Enjoy 主要特点
- 消灭传统模板引擎中大量繁杂概念，仅七个核心指令，学习成本极低
- 独创 DKFF 词法分析算法与 DLRD 语法分析算法，避免使用 javacc、antlr
- 与 java 打通式设计，在模板中与 java 交互极为方便
- 贴近 java 使用直觉，为 java 开发者量身打造
- 功能强大，极为简单覆盖掉 freemarker、velocity 的核心功能
- 扩展性强，支持多种扩展方式，且是唯一支持指令级扩展的模板引擎
- 回归模板引擎渲染 View 数据的本质，采用指令式设计，避免 view 层表达复杂逻辑
- 体积小，仅 227 KB，且不依赖于任何第三方


#### 简单示例：

**1. 与 Spring boot 整合**
```java
@Configuration
public class SpringBootConfig {

    @Bean(name = "jfinalViewResolver")
    public JFinalViewResolver getJFinalViewResolver() {

        // 创建用于整合 spring boot 的 ViewResolver 扩展对象
        JFinalViewResolver jfr = new JFinalViewResolver();

        // 对 spring boot 进行配置
        jfr.setSuffix(".html");
        jfr.setContentType("text/html;charset=UTF-8");
        jfr.setOrder(0);

        // 获取 engine 对象，对 enjoy 模板引擎进行配置，配置方式与前面章节完全一样
        Engine engine  = JFinalViewResolver.engine;

        // 热加载配置能对后续配置产生影响，需要放在最前面
        engine.setDevMode(true);

        // 使用 ClassPathSourceFactory 从 class path 与 jar 包中加载模板文件
        engine.setToClassPathSourceFactory();

        // 在使用 ClassPathSourceFactory 时要使用 setBaseTemplatePath
        // 代替 jfr.setPrefix("/view/")
        engine.setBaseTemplatePath("/view/");

        // 添加模板函数
        engine.addSharedFunction("/common/_layout.html");
        engine.addSharedFunction("/common/_paginate.html");

        // 更多配置与前面章节完全一样
        // engine.addDirective(...)
        // engine.addSharedMethod(...);

        return jfr;
    }
}
```

**2. 与 Spring  MVC 整合**

```java
<bean id="viewResolver" class="com.jfinal.template.ext.spring.JFinalViewResolver">
	<!-- 是否热加载模板文件 -->
	<property name="devMode" value="true" />
	<!-- 配置shared function，多文件用逗号分隔 -->
	<property name="sharedFunction" value="/view/_layout.html, /view/_paginate.html" />
	
	<!-- 是否支持以 #(session.value) 的方式访问 session -->
	<property name="sessionInView" value="true" />
	<property name="prefix" value="/view/" />
	<property name="suffix" value=".html" />
	<property name="order" value="1" />
	<property name="contentType" value="text/html; charset=utf-8" />
</bean>
```

**3.详细使用方法见官方文档**

read me 正在补充，详细使用文档见官网：[https://www.jfinal.com/doc/6-1](https://www.jfinal.com/doc/6-1)

**JFinal Enjoy 官方文档：[https://www.jfinal.com/doc/6-1](https://www.jfinal.com/doc/6-1)**





